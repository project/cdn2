<?php

/*
 * @file libcdn2.inc
 */
require_once("Crypt/HMAC.php");

class ErrorHandlingSoapClient {
  private $soapImpl;

  public function __construct($soapImpl) {
    $this->soapImpl = $soapImpl;
  }

  public function __call($function, $args) {
    try {
      return call_user_func_array(array($this->soapImpl, $function), $args);
    } catch (Exception $e) {
      return array('error' => $e->getMessage());
    } 
  }
}

interface CDN2SoapClient {
  public function getVideoToken($formats);
  public function getFormatPresets();
  public function getAssetsForVideoToken($videoToken);
  public function getVideosForSite();
  public function getVideoUploadStatus($videoToken);
  public function reencodeVideo($videoToken, $formats = array());
  public function deleteVideo($videoToken);
}

class CDN2SoapClientImpl implements CDN2SoapClient {
  static $instance;
  private $_siteToken;
  private $_secretKey;
  private $_hmac;
  private $_endpointUrl;

  public static function getInstance($endPointUrl, $siteToken, $secretKey) {
    if (self::$instance == NULL) {
            self::$instance = new CDN2SoapClientImpl($endPointUrl, $siteToken, $secretKey);
    }
    return self::$instance; 
  }
  
  private function CDN2SoapClientImpl($endPointUrl, $siteToken, $secretKey) {
    $this->_endPointUrl = $endPointUrl;
    $this->_siteToken = $siteToken;
    $this->_secretKey = $secretKey;
    $this->_hmac = new CDN2_Crypt_HMAC($this->_secretKey);
    $this->_soapClient = new SoapClient($this->_endPointUrl, array('classmap' => array('authenticationToken' => 'CDN2AuthenticationToken'), 'trace' => 1));
    
  }

  public function getVideoToken($formats) {
    return $this->_soapClient->getVideoToken(array('authenticationToken' => $this->createDigestAuthenticationToken(), 'siteToken' => $this->_siteToken, 'formats' => $formats))->return;
  }

  public function getFormatPresets() {
    $result = $this->_soapClient->getFormatPresets(array('authenticationToken' => $this->createDigestAuthenticationToken()))->return;
    return $result;
  }

  public function getAssetsForVideoToken($videoToken) {
    return $this->_soapClient->getAssetsForVideoToken(array('authenticationToken' => $this->createDigestAuthenticationToken(), 'videoToken' => $videoToken))->return;
  }

  public function getVideosForSite() {
    return $this->_soapClient->getVideosForSite(array('authenticationToken' => $this->createDigestAutheticationToken()))->return;
  }
  public function getVideoUploadStatus($videoToken) {
    return $this->_soapClient->getVideoUploadStatus(array('authenticationToken' => $this->createDigestAuthenticationToken(), 'videoToken'=> $videoToken))->return;
  }
  public function reencodeVideo($videoToken, $formats = array()) {
    return $this->_soapClient->reencodeVideo(array('authenticationToken' => $this->createDigestAuthenticationToken(), 'videoToken' => $videoToken, 'formats' => $formats))->return;
  }
  public function deleteVideo($videoToken) {
    return $this->_soapClient->reencodeVideo(array('authenticationToken' => $this->createDigestAuthenticationToken(), 'videoToken' => $videoToken))->return;
  }
  private function createDigestAuthenticationToken() {
    $timeStamp = date(DATE_RFC3339);
    $this->_hmac->setKey($this->_secretKey);
    $this->_hmac->setFunction("sha1");
    
    $calculatedHash = $this->_hmac->hash($this->_siteToken . $timeStamp);
    $authToken = new CDN2AuthenticationToken();
    $authToken->siteToken = $this->_siteToken;
    $authToken->timeStamp = $timeStamp;
    $authToken->calculatedHash = $calculatedHash;

    return new SoapVar($authToken, SOAP_ENC_OBJECT, 'cdn2AuthenticationToken', 'http://xml.radiantcdn.com/transcode');
  }
}

class CDN2AuthenticationToken {
  public $siteToken;
  public $timeStamp;
  public $calculatedHash;
  
}

class CDN2_Crypt_HMAC extends Crypt_HMAC {
    /** override default HMAC support since it uses a hex string and we want base64 */
    function hash($data) {
        $func = $this->_func;
        $rawHash = $func($this->_opad . pack($this->_pack, $func($this->_ipad . $data)), true);
        return base64_encode($rawHash);
    }

}
