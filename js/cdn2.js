
$(document).ready(function() {
  var oDom = $('div.cdn2_tabs > ul');
  if (oDom.length > 0) {
    oDom.tabs({
      select: function(e) {
        var panel = $(e.panel);
        var outer = panel.parent('div');
        outer.css({'min-height': outer.height() + 'px' });
      },
      show: function(e) {
        var panel = $(e.panel);
        var outer = panel.parent('div');
        outer.css({'min-height': panel.height() + 'px' }); 
      }
    });
    var nIndex = parseInt(aPresetList[cdn2_default_preset]);  // this is '1'
    oDom.tabs('select', nIndex);
  }
});
