$(document).ready(
function() {
  $('#progress-bar').progressBar({ barImage: cdn2_progress_image, boxImage: cdn2_progress_boximage, width: 298 });
  $('form#upload').submit(
    function() {
      $(window.parent.document).find('input#edit-submit').attr('disabled', 'true');
      setTimeout("cdn2_update()", 500);
    }
  );
}
);

function cdn2_update() {
    $("#progress-bar").show();
    $("#progress-info").show();
    $("div.form-item").hide();
    var vidid = $('#videoToken').val();
   $.ajax({
   type: "GET",
   url: "?q=cdn2/uploadform/status/"+vidid,
   success: function(msg) {
     $('#progress-bar').progressBar(Math.ceil(msg));
     if(msg != 100) {
      setTimeout("cdn2_update()", 500);
     }
   }
 });
}
