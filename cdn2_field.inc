<?php

//
// @file cdn2_field.inc configuration settings for the cck content type
//

/**
 * Declare information about the cdn2 field type
 */
function cdn2_field_info() {
  return array(
    'cdn2' => array('label' => 'CDN2'),
  );
}


function cdn2_widget_info() {
  return array(
    'cdn2' => array(
      'label' => 'CDN2 Field',
      'field types' => array('cdn2'),
    ),
  );
}

/**
 * Handle the parameters for the cck field. (used in the add/modify field form)
 */
function cdn2_field_settings($op, $field) {
  switch ($op) {
    // case 'form': (not needed.. no additional options for this field type)
    // case 'validate':
    // case 'save':
    case 'database columns':
      return array(
        'value' => array(
          'type' => 'varchar(36)',
          'not null' => TRUE,
          'default' => "''",
          'sortable' => FALSE
        )
      );
    case 'callbacks':
      // use our own view callback when rendering the field.
      return array(
        'view' => CONTENT_CALLBACK_CUSTOM,
      );
    // case 'tables':
    // case 'arguments':
    // case 'filters':
  }
}

/**
 * Define the behavior of the cdn2 field type
 */
function cdn2_field($op, &$node, $field, &$node_field, $teaser, $page) {
  switch ($op) {
    // case 'view':
    // case 'validate':
    // case 'submit':
    // case 'insert':
    // case 'update':
    // case 'delete':
    case 'load':
      $query = 'SELECT cv.video_token, cv.preset_name, cv.status, cv.asset_fetch_url, cv.file_size, cv.video_length FROM {cdn2_videos} cv WHERE cv.nid=%d';
      $resultset = db_query($query, $node->nid);
      $field_name = $field['field_name'];
      $additions = array();
      $additions[$field_name][0]['assets'] = array();
      while ($row = db_fetch_array($resultset)) {
        $additions[$field_name][0]['assets'][] = $row;
        // TODO: see if this is handled by the node load directly, otherwise
        // fetch it from the content table.
        $additions[$field_name][0]['value']= $row['video_token'];
      }

      $query = "SELECT nid from {cdn2_video_node} cvn WHERE video_token='%s'";
      $cvn_nid = db_result(db_query($query, $additions[$field_name][0]['value']));
      
      if ($cvn_nid != 0 && !count($additions[$field_name][0]['assets'])) {
        $additions[$field_name][0]['processing'] = TRUE;
      } 
      return $additions;
    case 'view':
      // process the item through cdn2_video theme (which handles different formats and preset types
      // through sub-element callbacks
      $field_name = $field['field_name'];
      if ($node->{$field_name} && $node->{$field_name}[0]['processing']) {
        $output = t('Your video is still processing.  Once it is complete it will be displayed here.');
      }
      else {
        $output = theme('cdn2_video', $node, $field, $node_field, $teaser, $page);
      }
      $node_field[0]['view'] = $output;

      $cdn2_default_preset = variable_get('cdn2_default_preset', $node_field[0]['assets'][0]['preset_name']);
      $js = 'var cdn2_default_preset = \''. $cdn2_default_preset .'\';'."\n";
      $js .='var aPresetList = new Array();'."\n";
      for ($i = 0; $i < count($items[0]['assets']); $i++) {
        $js .= sprintf('aPresetList[\'%s\'] = \'%s\';'."\n", $node_field[0]['assets'][$i]['preset_name'], $i);
      }
      drupal_add_js($js, 'inline');
      drupal_add_css(drupal_get_path('module', 'cdn2') .'/cdn2.css');
      drupal_add_js(drupal_get_path('module', 'cdn2') .'/js/ui.core.js');
      drupal_add_js(drupal_get_path('module', 'cdn2') .'/js/ui.tabs.js');
      drupal_add_js(drupal_get_path('module', 'cdn2') .'/js/cdn2.js', 'footer');
      $node_field = theme('field', $node, $field, $node_field, $teaser, $page);
      return $node_field;
    case 'insert':
      // reassociate the video back with the node
      //
      foreach ($node->{$field['field_name']} as $cdn2_video) {
        $video_token = $cdn2_video['value'];
        // update the db records to reassociate the node ids with any videos that have been processed.
        // this will address a potential race condition vis a vis xmlrpc callback is invoked before the node
        // has been saved.
        //
      
        $query = "UPDATE {cdn2_video_node} SET nid=%d WHERE video_token='%s'";
        db_query($query, $node->nid, $video_token);

        $query = "UPDATE {cdn2_videos} SET nid=%d where video_token='%s'";
        db_query($query, $node->nid, $video_token);
      }
      
      drupal_set_message('Your video is being processed.  It will appear here once it has finished processing.  You may '. l('refresh this page', 'node/'. $node->nid) .' to see progress.', 'info');
      break;
  }
}


/**
 * Implementation of hook_field_formatter().
 */
function cdn2_field_formatter($field, $item, $formatter, $node) {
  if (!isset($item['value'])) {
    return '';
  }

  if ($allowed_values = text_allowed_values($field)) {
    return $allowed_values[$item['value']];
  }

  switch ($formatter) {
    case 'plain':
      $text = strip_tags($item['value']);
      break;

    case 'trimmed':
      $text = node_teaser($item['value'], $field['text_processing'] ? $item['format'] : NULL);
      break;

    case 'default':
      $text = $item['value'];
  }

  if ($field['text_processing']) {
    return check_markup($text, $item['format'], is_null($node) || isset($node->in_preview));
  }
  else {
    return check_plain($text);
  }
}


/**
 * Implementation of hook_widget().
 * (shown when the edit/submit forms are called)
 */
function cdn2_widget($op, &$node, $field, &$node_field) {
  switch ($op) {
    case 'form':
      drupal_add_css(drupal_get_path('module', 'cdn2') .'/cdn2.css');
      $form = array();
      $formats = array();
      
      $enabled_presets = variable_get('cdn2_allowed_presets', array());
      foreach ($enabled_presets as $preset) {
        if ($preset && user_access('transcode cdn2 video to '. $preset)) {
          $formats[] = $preset;
        }
      }
      // refetch the video token for this node if it exists
      if ($node->nid) {
        $video_token = db_result(db_query("SELECT video_token from {cdn2_video_node} WHERE nid=%d", $node->nid));
      }
      else if (!count($_POST) && !isset($_POST[$field['field_name']])) {
        $cdn2 = _cdn2_get_soap_client();
        $video_token = $cdn2->getVideoToken($formats);
        $node_field[0]['value'] = $video_token;
      } 
      else {
         $video_token = $_POST[$field['field_name']][0]['value']; 
      }
      if (!$video_token && strpos('admin', $_REQUEST['q']) !== 0) {
        drupal_set_message(t('Unable to contact CDN2 service. Video Uploads are currently disabled.'), 'error');
        return $form;
      } 
      
      $form[$field['field_name']] = array('#tree' => TRUE);
      $form[$field['field_name']][0]['value'] = array(
        '#type' => 'hidden',
        '#title' => t($field['widget']['label']),
        '#default_value' => $video_token,
        '#required' => $field['required'],
        '#description' => t($field['widget']['description']),
        '#maxlength' => $field['max_length'] ? $field['max_length'] : NULL,
        '#weight' => $field['widget']['weight'],
      );

      $query = "SELECT is_submitted FROM {cdn2_video_node} WHERE video_token='%s'";
      $is_submitted = db_result(db_query($query, $video_token));

      $form['cdn2_is_submitted'] = array(
        '#type' => 'hidden',
        '#value' => $is_submitted ? 'true' : 'false'
      );
      // TODO: Here we need to show the uploader form if there's no video associated with this node, or show the list of assets associated with it and their status otherwise
      //
      $show_form = TRUE;
      if ($node->nid) {
        $show_form = FALSE;
        $assets = $node_field[0]['assets'];
        $asset_output = '';
        $form[$field['field_name'] .'_fieldset'] = array(
          '#type' => 'fieldset',
          '#title' => t('CDN2 Video Assets'),
        );

        if (count($assets)) {
          $asset_items = array();
          $presets = _cdn2_get_available_presets();
          $source = array();
          $source['asset_fetch_url'] = 'http://us.cloudfront.cdn2.net/'. $node->{$field['field_name']}[0]['value'];
          $source_preset = new stdClass();

          $source_preset->friendlyName = 'Original Video';
          $asset_items[] = theme('cdn2_video_asset_generic', $node, $source, $source_preset);
          foreach ($assets as $asset) {
            $asset_items[] = theme('cdn2_video_asset_generic', $node, $asset, $presets[$asset['preset_name']]);
          }
          $output = theme('item_list', $asset_items);
        }
        else {
          $output = t('Your video is either still transcoding or no formats were selected.');
        }
        $form[$field['field_name'] .'_fieldset'][$field['field_name'] .'_markup'] = array(
          '#type' => 'markup',
          '#value' => $output
        );

      } 
      if ($show_form) {
        // TODO: handle resizing of the iframe to its child content's scroll height, e.g.:
        // document.getElementById('cdn2_iframe').height = document.getElementById('cdn2_iframe').contentWindow.document.body.scrollHeight;
        $form['uploader'] = array(
          '#type' => 'markup',
          '#value' => '<iframe id="cdn2_iframe" class="cdn2" height="350" width="100%" frameborder="0" src="'. url('cdn2/uploadform') .'/'. $video_token .'">CDN2 Video requires iframe support</iframe>',
        );
      }
      return $form;
    case 'validate':
      if ($node->cdn2_is_submitted == 'false') {
        form_set_error('uploader', t('You must upload a video to continue.'));
      }
      break;
    case 'process form values':
      //$node->cdn2 = array('value'=>$node->cdn2);
      break;
    }
}

